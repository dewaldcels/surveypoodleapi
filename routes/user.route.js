const { User, UserSession } = require('../models/models');
const router = require('express').Router();
const Response = require('../models/response.model');
const TokenTool = require('../models/token.model');

router.get('/', async (req, res) => {
    const r = new Response();
    try {
        r.data = await User.findAll();
    }
    catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);
});

router.get('/:user_id', async (req, res) => {

    const r = new Response();
    const { user_id } = req.params;

    try {
        r.data = await User.findByPk(user_id);
        if (r.data == null) {
            throw({ status: 404, message: 'User not found'});
        }
    } catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);

});

router.post('/login', async (req, res) => {

    const { user } = req.body;
    const r = new Response();

    try {

        if (!user) {
            throw ({ status: 400, message: 'No user data was received in the request.' });
        }
        if (!user.username || !user.password) {
            throw ({ status: 400, message: 'Please provide a username and password to login.' });
        }

        const loginUser = await User.findOne({
            attributes: ['user_id', 'username', 'password', 'created_at', 'updated_at'],
            where: {
                username: user.username
            }
        });

        if (loginUser == null || loginUser.active == 0) {
            throw ({ status: 401, message: `The user ${user.username} does not exist.` });
        }

        if (!User.comparePassword(user.password, loginUser.password)) {
            throw ({ status: 401, message: `You have provided incorrect login credentials for ${user.username}.` });
        }

        const existingSession = await UserSession.findOne({
            where: {
                user_id: loginUser.user_id,
                active: 1
            }
        });

        let token;

        if (existingSession == null) {
            token = TokenTool.sign({ user_id: loginUser.user_id });
            await UserSession.create({
                user_id: loginUser.user_id,
                token: token,
            });
        } else {
            token = existingSession.token;
        }

        r.data = {
            user: {
                username: loginUser.username,
                created_at: loginUser.created_at,
                updated_at: loginUser.updated_at
            },
            token: token
        };

    } catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);

});

router.post('/register', async (req, res) => {

    const { user } = req.body;
    const r = new Response();

    try {

        if (!user) {
            throw ({ status: 400, message: 'No user data was received in the request.' });
        }

        const plainPassword = user.password;
        user.password = User.generatePassword(plainPassword);
        const newUser = await User.create(user);
        const token = TokenTool.sign({ user_id: newUser.user_id });

        const userSession = await UserSession.create({
            user_id: newUser.user_id,
            token: token
        });

        r.data = {
            user: {
                user_id: newUser.user_id,
                username: newUser.username,
                created_at: newUser.created_at,
                updated_at: newUser.updated_at
            },
            token
        };

        r.status = 201;
    } catch (e) {
        r.setError(e);
    }
    return res.status(r.status).json(r);

});

module.exports = router;